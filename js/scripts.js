// ------------------------------------------------------------------------------------------ 
//  Left pannel functions
// ------------------------------------------------------------------------------------------
$(document).ready(function() {
    $(".sidebar-dropdown > .active-item").click(function() {
        event.preventDefault();
        $(".sidebar-submenu").slideUp(200);
        if (
          $(this)
            .parent()
            .hasClass("active")
        ) {
          $(".sidebar-dropdown").removeClass("active");
          $(this)
            .parent()
            .removeClass("active");
        } else {
          $(".sidebar-dropdown").removeClass("active");
          $(this)
            .next(".sidebar-submenu")
            .slideDown(200);
          $(this)
            .parent()
            .addClass("active");
        }
    });
});
$(document).ready(function() {
     $(".sidebar-dropdown-2 > .active-item-2").click(function() {
         event.preventDefault();
        $(".sidebar-submenu-2").slideUp(200);
        if (
          $(this)
            .parent()
            .hasClass("active")
        ) {
          $(".sidebar-dropdown-2").removeClass("active");
          $(this)
            .parent()
            .removeClass("active");
        } else {
          $(".sidebar-dropdown-2").removeClass("active");
          $(this)
            .next(".sidebar-submenu-2")
            .slideDown(200);
          $(this)
            .parent()
            .addClass("active");
        }
    });
});

$(document).ready(function() {
    $("#close-sidebar").click(function() {
        $(".sidebar-custom").removeClass("toggled");
    });
    $("#show-sidebar").click(function() {
    $(".sidebar-custom").addClass("toggled");
    });  
});

// ------------------------------------------------------------------------------------------ 
//   Filter functions
// ------------------------------------------------------------------------------------------

$(document).ready(function(){
    $('.entries-custom .dropdown-menu').find('a').click(function(e) {
		e.preventDefault();
		var concept = $(this).text();
		$('.filter-select-custom span#entries-selected').text(concept);
        $('.filter-select-custom span#entries-selected').css('color', '#7499cc');
	});
});
$(document).ready(function(){
    $('.list-custom .dropdown-menu').find('a').click(function(e) {
		e.preventDefault();
		var concept = $(this).text();
        $('.filter-select-custom span#list-selected').text(concept);
        $('.filter-select-custom span#list-selected').css('color', '#7499cc');
	});
});
$(document).ready(function(){
    $('.grade-custom .dropdown-menu').find('a').click(function(e) {
		e.preventDefault();
		var concept = $(this).text();
        $('.filter-select-custom span#grade-selected').text(concept);
        $('.filter-select-custom span#grade-selected').css('color', '#7499cc');
	});
});
$(document).ready(function(){
    $('.starred-custom .dropdown-menu').find('a').click(function(e) {
		e.preventDefault();
		var concept = $(this).text();
        $('.filter-select-custom span#starred-selected').text(concept);
        $('.filter-select-custom span#starred-selected').css('color', '#7499cc');
	});
});

$(document).ready(function(){
    $('.grade-list-custom .dropdown-menu').find('a').click(function(e) {
		e.preventDefault();
		var concept = $(this).text();
        $('.filter-select-custom span#grade-list-selected').text(concept);
	});
});

// ------------------------------------------------------------------------------------------ 
//   Disable closing the dropdown when click inside
// ------------------------------------------------------------------------------------------
$(document).ready(function() { 
    $('.profile-custom .dropdown-menu').on('click', function(event){
        event.stopPropagation();
    });
});
    
    
// ------------------------------------------------------------------------------------------ 
//   Add modal insde the main-content
// ------------------------------------------------------------------------------------------
$(document).ready(function() {
    
    $("body").on("click",".action-openmsg",function(){
        $('#myModal2').modal('show');
        
        $(".main-content").addClass("after-modal-appended");
      //appending modal background inside the bigform-content
        $('.modal-backdrop').appendTo('.main-content');
      //removing body classes to able click events
        $('body').removeClass('modal-open');
    });
    
});


    
    
    
    
    
    
    



